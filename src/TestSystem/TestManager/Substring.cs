﻿namespace TestSystem.TestManager
{
    class Substring
    {
        private readonly char[] _chars;

        public string Current => new string(_chars);

        private static void InitFrom(char[] chars, int i)
        {
            for (int j = i; j < chars.Length; j++)
            {
                chars[j] = 'a';
            }
        }

        public Substring(int length)
        {
            _chars = new char[length];
            InitFrom(_chars, 0);
        }

        public bool GetNext()
        {
            bool change = false;
            int p = _chars.Length - 1;
            bool needClear = false;
            while (!change)
            {
                if (_chars[p] < 'z')
                {
                    _chars[p]++;
                    if (needClear)
                        InitFrom(_chars, p + 1);
                    change = true;
                }
                else
                {
                    if (p > 0)
                    {
                        p--;
                        needClear = true;
                    }
                    else
                        break;
                }
            }
            return change;
        }
    }
}
