﻿using System.Collections.Generic;
using TestSystem.Common;

namespace TestSystem.SimpleSolve
{
    /// <summary>
    /// Типовое решение основанное на последовательном просмотре всех пользователей и проверке 
    /// запроса. Будем считать его эталонным. И оценивать остальные решения по отношению к нему. 
    /// </summary>
    public class SimpleUserManager : IUserManager
    {
        public string Name => "SimpleManager";

        class UserWrapper
        {
            public UserWrapper(User user)
            {
                User = user;
                Update(user);
            }
            
            public string Data;

            public User User;

            public void Update(User user)
            {
                Data = user.Login.ToLower() + " " + user.Email.ToLower() + " " + user.Phone.ToLower();
            }
        }

        /// <summary>
        /// Структура для хранения всех пользователей
        /// </summary>
        private readonly SortedList<int, UserWrapper> _users
            = new SortedList<int, UserWrapper>();        

        public void Add(User user) => _users.Add(user.Id, new UserWrapper(user));
        
        public void Update(User user) 
        {
            if (_users.ContainsKey(user.Id))
                _users[user.Id].Update(user);
        }        
        
        public List<User> Find(string query, int maxCount)
        {            
            query = query.ToLower();            
            var lst = new List<User>();
            foreach (var userWrapper in _users.Values) // Тут не учитывается потокобезопасность. для теста не критично.
            {
                if (userWrapper.Data.Contains(query))
                    lst.Add(userWrapper.User);

                if (lst.Count == maxCount)
                    break;
            }
            return lst;
        }       
    }
}
