﻿using System;
using System.Text;
using TestSystem.Common;

namespace TestSystem.CandidatSolve.Base
{
    public class SimpleUserSearchInfo : BaseUserWrapper, IComparable
    {
        public SimpleUserSearchInfo(User user) : base(user)
        {
            if (user == null)
                throw new ArgumentNullException();
        }

        /// <summary>
        /// Склееенные данные для поиска
        /// </summary>
        public string SearchData { get; private set; }

        public override void Update(User user)
        {
            if(user == null)
                throw new ArgumentNullException();

            var builder = new StringBuilder();

            if (!string.IsNullOrEmpty(user.Login))
            {
                builder.Append(user.Login.ToUpperInvariant());
                builder.Append(UserConstants.WHITE_SPACE);
            }

            builder.Append(user.Email.ToUpperInvariant());
            builder.Append(UserConstants.WHITE_SPACE);

            if (!string.IsNullOrEmpty(user.Phone))
                builder.Append(user.Phone);

            SearchData = builder.ToString();
        }


        public override int GetHashCode()
        {
            return User.Id.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var userWrapper = obj as SimpleUserSearchInfo;

            if (userWrapper == null)
                return false;

            return User.Id == userWrapper.User.Id;
        }

        public int GetId()
        {
            return User.Id;
        }

        public int CompareTo(object obj)
        {
            var wrapper = obj as SimpleUserSearchInfo;
            if (wrapper == null)
                return -1;

            if (wrapper.User.Id == User.Id)
                return 0;

            if (User.Id < wrapper.User.Id)
                return -1;

            return 1;
        }
    }
}
