﻿using System.Collections.Generic;
using System.Linq;
using TestSystem.CandidatSolve.Base;
using TestSystem.Common;

namespace TestSystem.CandidatSolve.SplitLexems
{
    public class SplitLexemsRepositoryFromPolikarpov : BaseUserRepository<SimpleUserSearchInfo>
    {
        #region Поля, константы, конструктор

        private readonly int[] _lexemSizes;
        private readonly LexemUtility _lexemUtility;

        //  Ключ - лексема
        //  Значение - сортированный список юзеров по Id, у которых есть эта лексема.
        private readonly Dictionary<string, UsersChunk> _containsLexems;

        public SplitLexemsRepositoryFromPolikarpov()
        {
            _lexemUtility = new LexemUtility();
            _lexemSizes = new[]
            {
                8, 4, 3, 2, 1
            };

            _containsLexems = new Dictionary<string, UsersChunk>();
        }

        #endregion



        public override string Name
        {
            get { return nameof(SplitLexemsRepositoryFromPolikarpov); }
        }        

        protected override SimpleUserSearchInfo CreateUserWrapper(User user)
        {
            var wrapper = new SimpleUserSearchInfo(user);

            foreach (var lexemSize in _lexemSizes)
            {
                AppendLexem(_lexemUtility.FindAllLexems(user.Login?.ToUpperInvariant(), lexemSize), wrapper);
                AppendLexem(_lexemUtility.FindAllLexems(user.Email.ToUpperInvariant(), lexemSize), wrapper);
                AppendLexem(_lexemUtility.FindAllLexems(user.Phone.ToUpperInvariant(), lexemSize), wrapper);
            }            

            return wrapper;
        }

        protected override List<User> FindUsers(string query, int maxCount)
        {
            var upperCaseQuery = query.ToUpperInvariant();

            //Если длина поисковой строки равна одной из размеров лексем, то просто возвращаем юзеров в нужном кол-ве
            if (_lexemSizes.Contains(upperCaseQuery.Length))
            {
                if (!_containsLexems.ContainsKey(upperCaseQuery))
                    return new List<User>();

                return _containsLexems[upperCaseQuery].GetUsers(maxCount);
            }

            //Разбиваем поисковую строку на лексемы. Если хотя бы одной лексемы нет, значит и юзеров нет таких
            var queryLexems = _lexemUtility.SplitToLexems(upperCaseQuery, _lexemSizes);
            var chunks = new List<UsersChunk>();

            foreach (var queryLexem in queryLexems)
            {
                if(!_containsLexems.ContainsKey(queryLexem))
                    return new List<User>();

                chunks.Add(_containsLexems[queryLexem]);
            }

            // Даже если все лексемы есть, может быть ситуация, что все лексемы по отдельности есть, а общей строки нет
            var result = new SortedDictionary<int, User>();
            foreach (var chunk in chunks)
            {
                foreach (var user in chunk.UserWrappers)
                {
                    if (user.Value.SearchData.Contains(upperCaseQuery) && !result.ContainsKey(user.Value.GetId()))
                        result.Add(user.Value.GetId(), user.Value.User);                        
                }                
            }

            return result.Values.Take(maxCount).ToList();
        }


        protected override void OnUpdate(SimpleUserSearchInfo user)
        {
            var oldUser = _users[user.User.Id];
            UpdateValue(oldUser.User.Email, user.User.Email, oldUser, user);
            UpdateValue(oldUser.User.Login, user.User.Login, oldUser, user);
            UpdateValue(oldUser.User.Phone, user.User.Phone, oldUser, user);
        }

        private void UpdateValue(string oldValue, string newValue, SimpleUserSearchInfo oldUser, SimpleUserSearchInfo newUser)
        {
            if (!string.IsNullOrWhiteSpace(oldValue))
            {
                var oldUpperCase = oldValue?.ToUpperInvariant();
                var newUpperCase = newValue?.ToUpperInvariant();

                if (oldUpperCase != newUpperCase)
                {
                    if (_containsLexems.ContainsKey(oldUpperCase))
                    {
                        var chunk = _containsLexems[oldUpperCase];
                        chunk.Remove(oldUser);

                        if (chunk.Count == 0)
                            _containsLexems.Remove(oldUpperCase);
                    }

                    AppendLexem(new string[] {newUpperCase}, newUser);
                }                
            }
        }

        private void AppendLexem(IEnumerable<string> lexems, SimpleUserSearchInfo user)
        {
            foreach (var lexem in lexems)
            {
                if (!_containsLexems.ContainsKey(lexem))
                {
                    int? capacity = null;
                    // Для лексем длиной < 4 будет много записей в любом случае
                    if (lexem.Length < 4)
                        capacity = UserConstants.LIMIT_MAX;

                    var chunk = new UsersChunk(capacity);
                    chunk.AddOrReplace(user);
                    _containsLexems.Add(lexem, chunk);
                    continue;
                }

                _containsLexems[lexem].AddOrReplace(user);
            }
        }

    }
}
