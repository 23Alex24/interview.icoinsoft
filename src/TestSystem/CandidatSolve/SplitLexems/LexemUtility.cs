﻿using System.Collections.Generic;

namespace TestSystem.CandidatSolve.SplitLexems
{
    public class LexemUtility
    {
        /// <summary>
        /// Ищет в строке все лексемы указанной длины
        /// </summary>
        /// <param name="value">Строка, в которой нужно найти все лексемы</param>
        /// <param name="lexemLength">Длина лексем</param>
        public IEnumerable<string> FindAllLexems(string value, int lexemLength)
        {
            if(string.IsNullOrWhiteSpace(value))
                yield break;

            if (lexemLength == 1)
            {
                foreach (var symbol in value)
                {
                    yield return symbol.ToString();
                }

                yield break;
            }

            int i = 0;
            while (i + lexemLength <= value.Length)
            {
                yield return value.Substring(i, lexemLength);
                i++;
            }

            yield break;
        }

        /// <summary>
        /// Разбивает строку на лексемы. Сначала пытается разбить строку по первым по порядку размерам лексем.
        /// Если после этого остается неразбитая часть берет следующий размер лексем и доразбивает по ним.
        /// Данный метод НЕ ищет все возможные лексемы, а только делает разовую разбивку строки
        /// </summary>
        /// <param name="value">Строка, которая будет разбита</param>
        /// <param name="lexemLengths">Коллекция длинн лексем. Порядок элементов влияет на результат работы</param>
        public IEnumerable<string> SplitToLexems(string value, int[] lexemLengths)
        {
            if (string.IsNullOrWhiteSpace(value) || lexemLengths.Length == 0)
                yield break;

            int i = 0;
            int lengthIndex = 0;
            while (lengthIndex < lexemLengths.Length && i < value.Length)
            {
                var length = lexemLengths[lengthIndex];
                if (length < 1)
                    yield break;

                if (length == value.Length)
                {
                    yield return value;
                    yield break;
                }

                while (i + length <= value.Length)
                {
                    yield return value.Substring(i, length);
                    i = i + length;
                }

                lengthIndex++;
            }

            yield break;
        }
    }
}
