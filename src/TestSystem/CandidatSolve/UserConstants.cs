﻿namespace TestSystem.CandidatSolve
{
    public static class UserConstants
    {
        /// <summary>
        /// Min value of user id
        /// </summary>
        public const int MIN_ID = 1;

        public const int LOGIN_MAX_LENGTH = 64;

        public const int EMAIL_MAX_LENGTH = 128;

        public const int PHONE_MAX_LENGTH = 16;

        public const int EMAIL_MIN_LENGTH = 1;

        public const string WHITE_SPACE = " ";

        public const int MAX_USERS_COUNT = 500000;

        /// <summary>
        /// Максимальное кол-во юзеров для выдачи в методе Find. Данное значение не должно быть меньше 2
        /// </summary>
        public const int LIMIT_MAX = 500;

        public class Errors
        {
            public const string USER_ALREADY_ADDED = "User {0} already added";

            public static string MAXUMUM_USERS_ADDED = $"Maximum users count is {MAX_USERS_COUNT}";

            public const string QUERY_CANT_CONTAINS_WHITE_SPACES = "Query can't contains white spaces";

            public const string USER_NOT_FOUND = "User {0} not found";
        }
    }
}
