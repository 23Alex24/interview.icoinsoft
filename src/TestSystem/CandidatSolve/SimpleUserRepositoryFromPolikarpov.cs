﻿using System.Collections.Generic;
using System.Text;
using TestSystem.CandidatSolve.Base;
using TestSystem.Common;

namespace TestSystem.CandidatSolve
{
    public class SimpleUserRepositoryFromPolikarpov : BaseUserRepository<SimpleUserSearchInfo>
    {
        public override string Name
        {
            get { return nameof(SimpleUserRepositoryFromPolikarpov); }
        }

        protected override SimpleUserSearchInfo CreateUserWrapper(User user)
        {
            return new SimpleUserSearchInfo(user);
        }

        protected override List<User> FindUsers(string query, int maxCount)
        {
            var upperCaseQuery = query.ToUpperInvariant();

            var result = new List<User>();
            foreach (var userSearchInfo in _users.Values)
            {
                if (userSearchInfo.SearchData.Contains(upperCaseQuery))
                    result.Add(userSearchInfo.User);

                if (result.Count == maxCount)
                    return result;
            }

            return result;
        }        
    }
}
