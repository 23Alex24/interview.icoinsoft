# Тестовое задание для iCoinSoft

Выполнял в апреле 2019. Тестовое задание на оптимизацию скорости работы.

Само приложение прислали, код я писал только в папке CandidatSolve, весь остальной код - это тестовая система.

*  [Тестовое задание](https://gitlab.com/23Alex24/interview.icoinsoft/blob/master/docs/task.md)
*  [Решение задачи и разбор задания и ограничений](https://gitlab.com/23Alex24/interview.icoinsoft/blob/master/docs/solution.md)
*  [Feedback на мое решение](https://gitlab.com/23Alex24/interview.icoinsoft/blob/master/docs/feedback.md)
*  [Вопросы, которые я задавал по задаче и ответы на них](https://gitlab.com/23Alex24/interview.icoinsoft/blob/master/docs/questions.md)